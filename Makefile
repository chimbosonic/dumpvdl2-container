USB_DEVICE_FILE="$(shell ./scripts/get_usb_device.sh)"

IMAGE_NAME="dumpvdl2"

run:
	docker run -d --rm --device $(USB_DEVICE_FILE) -p 54000:54000  $(IMAGE_NAME)  
	
build:
	docker build --build-arg BUILD_DATE=$(shell date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg VCS_REF=$(shell git rev-parse --short HEAD) -t $(IMAGE_NAME) .

run-bash:
	docker run -it --rm --device $(USB_DEVICE_FILE) --entrypoint /bin/bash  $(IMAGE_NAME)