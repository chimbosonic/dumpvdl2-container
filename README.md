# dumpvdl2 Container (No Longer Maintained)
[![pipeline status](https://gitlab.com/chimbosonic/dumpvdl2-container/badges/master/pipeline.svg)](https://gitlab.com/chimbosonic/dumpvdl2-container/-/commits/master)

A simple container that runs [dumpvld2](https://github.com/szpajder/dumpvdl2) with SDRplay patch and shares the output via [frontail](https://github.com/mthenw/frontail) on port 54000

Source code: https://gitlab.com/chimbosonic/dumpvdl2-container

Docker repo: https://hub.docker.com/r/chimbosonic/dumpvdl2

To run it you will need to share the usb device to the container:
```
docker run -d --rm --device ${USB_DEVICE_FILE} -p 54000:54000 chimbosonic/dumpvdl2

```

`${USB_DEVICE_FILE}` can be determined by using `lsusb` however for simplicity you can run:

```
./scripts/get_usb_device.sh
```

All credit goes to the maintainers of [dumpvld2](https://github.com/szpajder/dumpvdl2) and [frontail](https://github.com/mthenw/frontail)
