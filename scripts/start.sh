#!/bin/bash
DUMPVLD2_FREQ="136650000 136700000 136800000 136725000 136775000 136825000 136875000 136975000"
DUMPVDL2_ARGS="--sdrplay 0 --utc  --msg-filter all,-acars_nodata --bs-db /opt/basestation.sqb --addrinfo verbose"
FRONTAIL_ARGS="--disable-usage-stats"
FRONTAIL_PORT="54000"
function start() {
	dumpvdl2 ${DUMPVDL2_ARGS} ${DUMPVLD2_FREQ}  | frontail -p ${FRONTAIL_PORT} ${FRONTAIL_ARGS} -
}

start