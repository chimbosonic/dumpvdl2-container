#!/bin/bash
set -e
FRONTAIL_URL="https://github.com/mthenw/frontail/releases/download/v4.9.1/frontail-linux"
RSP_API_URL="https://www.sdrplay.com/software/SDRplay_RSP_API-Linux-2.13.1.run"
LIBACARS_URL="https://github.com/szpajder/libacars"
DUMPVDL2_URL="https://github.com/szpajder/dumpvdl2.git"
BUILD_DEPS="git libsqlite3-dev libglib2.0-dev zlib1g-dev libxml2-dev wget pkg-config ca-certificates libpsl5 openssl libssl1.1 build-essential cmake"
DEPS="zlib1g libglib2.0-0 libsqlite3-0 libxml2 librtlsdr0 libusb-1.0-0 rtl-sdr"
CUR_DIR=$(pwd)

function install_deps() {
	apt-get update
	apt-get install -y ${BUILD_DEPS} --no-install-recommends
	apt-get install -y ${DEPS} --no-install-recommends
}

function install_frontail(){
	wget ${FRONTAIL_URL} -O /usr/local/bin/frontail
	chmod +x /usr/local/bin/frontail
}

function install_rsp_api() {
	TEMP_DIR=$(mktemp -d)
	wget ${RSP_API_URL} -O ${TEMP_DIR}/rsp_api.run
	chmod +x ${TEMP_DIR}/rsp_api.run
	cd ${TEMP_DIR}
	./rsp_api.run --noexec --tar -xvf
	cp ./x86_64/libmirsdrapi-rsp.so.2.13 /usr/local/lib/.
	cp ./mirsdrapi-rsp.h /usr/local/include/.
	chmod 644 /usr/local/lib/libmirsdrapi-rsp.so.2.13 /usr/local/include/mirsdrapi-rsp.h	
	ln -s /usr/local/lib/libmirsdrapi-rsp.so.2.13 /usr/local/lib/libmirsdrapi-rsp.so.2
	ln -s /usr/local/lib/libmirsdrapi-rsp.so.2 /usr/local/lib/libmirsdrapi-rsp.so
	ldconfig
	cd ${CUR_DIR}
	rm -rf ${TEMP_DIR}
}

function install_lib_acars() {
	TEMP_DIR=$(mktemp -d)
	cd ${TEMP_DIR}
	git clone ${LIBACARS_URL}
	mkdir -p ./libacars/build
	cd ./libacars/build
	cmake ../
	make
	make install
	ldconfig
	cd ${CUR_DIR}
	rm -rf ${TEMP_DIR}
}

function install_dumpvdl2() {
	TEMP_DIR=$(mktemp -d)
	cd ${TEMP_DIR}
	git clone ${DUMPVDL2_URL}
	mkdir -p ./dumpvdl2/build
	cd ./dumpvdl2/build
	cmake ../
	make
	make install
	ldconfig
        cd ${CUR_DIR}
        rm -rf ${TEMP_DIR}
}

function install_basestation_db() {
	mkdir -p /opt/
	wget https://data.flightairmap.com/data/basestation/BaseStation.sqb.gz -O /opt/basestation.sqb.gz
	gzip -d /opt/basestation.sqb.gz
}

function cleanup() {
	apt-get purge -y ${BUILD_DEPS}
	apt-get autoremove -y
	apt-get clean all
	rm -rf /var/lib/apt/lists/*
	rm -rf /tmp/**
}

install_deps
install_frontail
install_rsp_api
install_lib_acars
install_dumpvdl2
install_basestation_db
cleanup
